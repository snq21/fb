<?php
require_once('mysql.php');
class Table extends Mysql{
    public function Json($var)
    {
        return json_encode($var);
    }
    public function getAll($tableName)
    {
        return  $this->select("select * from {$tableName}")->fetch_all(MYSQLI_ASSOC);
        
    }
    public function getBystr($tableName,$str='id=1')
    {   
        return  $this->select("select * from {$tableName} where {$str}")->fetch_all(MYSQLI_ASSOC);
    }
    public function clear($text)
    {
        $text=$this->connect()->escape_string($text);
        $text=htmlspecialchars($text);
        return $text;
    }
    public function rawget($sql)
    {   
        return  $this->select($sql)->fetch_all(MYSQLI_ASSOC);
    }
    public function rawAdd($sql)
    {   
        return  $this->insert($sql);
    }
    public function update($tableName,$set,$where)
    {   
        return  $this->insert("update {$tableName} set {$set} where {$where}");
    }
    public function Add($tableName,$cols,$vals)
    {   
        return  $this->insert("insert into {$tableName} ({$cols})  VALUES ({$vals})");
    }

}
