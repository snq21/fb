<?php
class Mysql
{
    public $ct;
    public $db;

    public function __construct()
    {
        $this->db=  [
                     'host'=>'localhost',
                     'user'=>'',
                     'password'=>'',
                     'db'=>'fbdb'
                    ];

    }

    public function connect()
    {
        $this->ct = @new mysqli($this->db['host'],$this->db['user'],$this->db['password'],$this->db['db']);
        $this->ct->set_charset('utf8');

        if ($this->ct->connect_error)
        {
            return $this->ct->connect_error;

        }else
        {
            return $this->ct;
        }
    }
    public function select($sql)
    {
        return $this->connect()->query($sql);
    }
    public function insert($sql)
    {
        $result = $this->connect()->query($sql);
        if($result){
            return ['affected'=>$this->ct->affected_rows,'insert_id'=>$this->ct->insert_id];
        }else{
            return ['error'=>$this->ct->error];
        }   
    }
};


