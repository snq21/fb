<?php
global $secretKey;
global $Gtable;
global $autoRulesCop;
global $apiAddress;
if(isset($_GET['json'],$_GET['get-token_id']))
{
header('Content-Type: application/json');
$tid=(int)$_GET['get-token_id'];
$tokenText=$Gtable->getBystr('Tokens',"id={$tid} and u_id={$_SESSION['uid']} and status=0");
if (count($tokenText)>0) {
    $autoRulesCop=new AutoRulesCopier($tokenText[0]['token'],$apiAddress,$tokenText[0]['proxy_url'],$tokenText[0]['proxy_auth'],$tokenText[0]['useragent']);
    $accounts=$autoRulesCop->GetAccounts();
    echo $accounts;
}else{
    echo json_encode(['msg'=>'не найден токен!']);
}

die();
}

if(isset($_GET['json'],$_GET['account_id'],$_GET['token_id']))
{
header('Content-Type: application/json');
$tid=(int)$_GET['token_id'];
$account_id=$Gtable->clear($_GET['account_id']);
$tokenText=$Gtable->getBystr('Tokens',"id={$tid} and u_id={$_SESSION['uid']} and status=0");
if (count($tokenText)>0) {
    $autoRulesCop=new AutoRulesCopier($tokenText[0]['token'],$apiAddress,$tokenText[0]['proxy_url'],$tokenText[0]['proxy_auth'],$tokenText[0]['useragent']);
    $accounts=$autoRulesCop->DownloadInterface($account_id);
    //$accounts=$autoRulesCop->Upload($account_id);
    echo ($accounts);
}else{
    echo json_encode(['msg'=>'не найден токен!']);
}
die();
}



if (isset($_POST['cabinet_id_rules'],$_POST['token_id'],$_POST['cabinet_id_source'],$_POST['cabinet_id_dest'])){
    

    
    $token_id=$Gtable->clear($_POST['token_id']);
    $cabinet_id_source=$Gtable->clear($_POST['cabinet_id_source']);
    $cabinet_id_dest=$_POST['cabinet_id_dest'];
    $cabinet_id_rules=$_POST['cabinet_id_rules'];
    $tokenText=$Gtable->getBystr('Tokens','id='.$token_id);
    if (count($tokenText)==1){

        $autoRulesCop=new AutoRulesCopier($tokenText[0]['token'],$apiAddress,$tokenText[0]['proxy_url'],$tokenText[0]['proxy_auth'],$tokenText[0]['useragent']);
        $cabinet_val=$autoRulesCop->DownloadInterface($cabinet_id_source);
        $cols = 'token_id, cabinet_id, cabinet_val, operation, cabinet_dest_id';
        $cabinet_id_deststr=implode(",",$cabinet_id_dest);
        $cv=base64_encode($cabinet_val);
        $vals = "{$token_id},'{$cabinet_id_source}','{$cv}',1,'{$cabinet_id_deststr}'";
        $Gtable->Add('cabinets',$cols,$vals);
        $cabinet_val=json_decode($cabinet_val,true);
        $cabinet_val=$cabinet_val['user_settings']['column_presets']['data'];

        foreach ($cabinet_id_dest as $cabinet_id){
            $temp_val=$autoRulesCop->DownloadInterface($cabinet_id,"user_settings");
            $temp_val=json_decode($temp_val,true);
            $usid =$temp_val['user_settings']['id'];
            
            if (empty($usid)) {
                $temp_val=$autoRulesCop->UploadInterface($cabinet_id,[]);
                $temp_val=json_decode($temp_val,true);
                $usid = $temp_val['id'];
                
            }

            foreach ($cabinet_val as $ck){
                $dataTemp=[];
                    $dataTemp['name']=$ck['name'];
                    $dataTemp["columns"]=$ck["columns"];
                echo $autoRulesCop->UploadColumn($usid,$dataTemp);
            }
        }
        Alert('Настройки скопированы');
    }else
        Alert('Не верный токен!');
}
loadV('tcolumn');