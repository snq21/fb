<?php

if (!isset($_SESSION['el'])) {
	$_SESSION['el']=file_get_contents('proxy.txt');
}

$arr=explode(PHP_EOL,$_SESSION['el']);

function queryHttp($url,$proxy,$auth=null)
{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		//curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
		curl_setopt($ch, CURLOPT_PROXY,$proxy);
		if (strlen($proxy)>0&&$auth!=null) {
			curl_setopt($ch, CURLOPT_PROXYUSERPWD, $auth);
		}
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$curl_scraped_page = curl_exec($ch);
		if(curl_exec($ch) === false)
		{
			$curl_scraped_page=curl_error($ch);
		}
		curl_close($ch);
		return $curl_scraped_page;
}

if(isset($_GET['json'],$_GET['proxy-url'])){
	$id=$_GET['proxy-url'];
	$auth=null;
	if (!empty($_GET['proxy-auth'])) {
		$auth=$_GET['proxy-auth'];		
	}
	if (!empty($id)) {
		$ip=queryHttp('https://vk-book.ru/my-ip.php',$auth.'@'.$id,null);
		if ($ip!=false&&false==strpos($_SERVER['HTTP_HOST'],$ip)) {
			echo "Данный прокси доступен для использования".$ip;
		}else{
			echo "Данный прокси не работает".$ip;
		}

	}else{
		echo "Пустой прокси сервер!!!";
	}


	die();
}

if(isset($_GET['json'],$_GET['proxy-line'])){
	$id=(int)$_GET['proxy-line'];
	$ip=queryHttp('https://vk-book.ru/my-ip.php',$arr[$id-1]);
	if ($ip!=false) {
		echo "Данный прокси доступен для использования".$arr[$id-1];
	}else{
		echo "Данный прокси не работает".$arr[$id-1];
	}
	die();

}

$table='
<div class="container-fluid p-0">
<div class="row justify-content-center p-5 bor">
	<div class="col-md-8 offset-1">
	<h2>Обновить список прокси серверов</h2>
		<form action="" class="form-inline p-5">
		<input type="text" name="proxy" class="form-control" placeholder="Ссылка на файл" required> 
		<button class="btn btn-success">Обновить список</button> 
		</form>	
	</div>
</div>

		<div class="row">
			<div class="col">
				<table class="table table-dark table-bordered">
				<tr><th> #</th>
 <th>IP</th>
 <th>Статус</th>
 </tr>';
 $i=1;
 
 foreach ($arr as $item) {
	 if (isset($_SESSION['proxy'])) {
		 if ($_SESSION['proxy']==$item) {
			$table.="<tr class='text-success'><td>{$i}</td><td>{$item}</td><td><a href='#' onclick='check({$i})'>Проверить</a></td></tr>";	 
		 }else{
			$table.="<tr ><td>{$i}</td><td>{$item}</td><td><a href='#' onclick='check({$i})'>Проверить</a></td></tr>";	 		
			 }
		 }else{
			$table.="<tr ><td>{$i}</td><td>{$item}</td><td><a href='#' onclick='check({$i})'>Проверить</a></td></tr>";	 
	 }
	
	
	$i++;
	
 }
$table.="</table>
</div>
</div>
</div>";
echo $table;

?>
<script>
function check(i) {
	document.location.href="/proxy?proxy-line="+i+"&json=true";
   	
}
</script>
