<?php
if (isset($_POST['token'],$_SESSION['uid'],$_POST['proxy_url'],$_POST['useragent'])) {
    global $Gtable;
    $token = $Gtable->clear($_POST['token']);
    $title = $Gtable->clear($_POST['title']);
    $proxy_auth = $Gtable->clear($_POST['proxy_auth']);
    $proxy_url = $Gtable->clear($_POST['proxy_url']);
    $useragent =$Gtable->clear($_POST['useragent']);
    
    if (isset($_POST['tid'])){
        $tid=(int)$_POST['tid'];
        $Gtable->update('Tokens',"token='{$token}',title='{$title}',proxy_auth='{$proxy_auth}',proxy_url='{$proxy_url}',useragent='{$useragent}'","id=".$tid);
        redirect('/tokens?msg='.urlencode('Токен обновлен'));
    }else{
        loadV('token');
    }
    if (!empty($token)&&!isset($_POST['tid']))
    {
        $result=$Gtable->getBystr('Tokens',"u_id={$_SESSION['uid']} and token='{$token}'");
        if (count($result)>0) {
            Alert('Вы уже добавляли данный токен!');
        }else{
            $cols="u_id,token,title,proxy_auth,proxy_url,status";
            $vals="{$_SESSION['uid']},'{$token}','{$title}','{$proxy_auth}','{$proxy_url}',0";
            $result=$Gtable->Add('Tokens',$cols,$vals);
            if (count($result)==2) {
                redirect('/tokens?msg='.urlencode('Токен добавлен в систему'));
            }else{
                Alert('Не удалось зарегестрировать токен  !');
            }
        }


    }
}else{
    loadV('token');
}
if (isset($_GET['ban-id'])) {
    $id=(int)$_GET['ban-id'];
    if ($id) {
        global $Gtable;
        $Gtable->update('Tokens',"status=1","id=".$id);
        redirect('/tokens?msg='.urlencode('Токен забанен!'));
    }
}
if (isset($_GET['razban-id'])) {
    $id=(int)$_GET['razban-id'];
    if ($id) {
        global $Gtable;
        $Gtable->update('Tokens',"status=0","id=".$id);
        redirect('/tokens?msg='.urlencode('Токен разблокирован!'));
    }
}
