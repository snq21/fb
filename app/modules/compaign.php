<?php
global $secretKey;
global $Gtable;
global $autoRulesCop;
global $apiAddress;
if(isset($_GET['json'],$_GET['get-token_id']))
{
header('Content-Type: application/json');
$tid=(int)$_GET['get-token_id'];
$tokenText=$Gtable->getBystr('Tokens',"id={$tid} and u_id={$_SESSION['uid']} and status=0");
if (count($tokenText)>0) {
    $autoRulesCop=new AutoRulesCopier($tokenText[0]['token'],$apiAddress,$tokenText[0]['proxy_url'],$tokenText[0]['proxy_auth'],$tokenText[0]['useragent']);
    $accounts=$autoRulesCop->GetAccounts();
    echo $accounts;
}else{
    echo json_encode(['msg'=>'не найден токен!']);
}

die();
}

if(isset($_GET['json'],$_GET['cmp-token_id']))
{
header('Content-Type: application/json');
$tid=(int)$_GET['cmp-token_id'];
$tokenText=$Gtable->getBystr('Tokens',"id={$tid} and u_id={$_SESSION['uid']} and status=0");
if (count($tokenText)>0) {
    $autoRulesCop=new AutoRulesCopier($tokenText[0]['token'],$apiAddress,$tokenText[0]['proxy_url'],$tokenText[0]['proxy_auth'],$tokenText[0]['useragent']);
    $accounts=$autoRulesCop->GetCompaigns();
    echo $accounts;
}else{
    echo json_encode(['msg'=>'не найден токен!']);
}

die();
}



if(isset($_GET['json'],$_GET['account_id'],$_GET['token_id']))
{
header('Content-Type: application/json');
$tid=(int)$_GET['token_id'];
$account_id=$Gtable->clear($_GET['account_id']);
$tokenText=$Gtable->getBystr('Tokens',"id={$tid} and u_id={$_SESSION['uid']} and status=0");
if (count($tokenText)>0) {
    $autoRulesCop=new AutoRulesCopier($tokenText[0]['token'],$apiAddress,$tokenText[0]['proxy_url'],$tokenText[0]['proxy_auth'],$tokenText[0]['useragent']);
    $accounts=$autoRulesCop->Download($account_id);
    //$accounts=$autoRulesCop->Upload($account_id);
    echo base64_decode($accounts);
}else{
    echo json_encode(['msg'=>'не найден токен!']);
}
die();
}



if (isset($_POST['cabinet_id_rules'],$_POST['token_id'],$_POST['cabinet_id_source'],$_POST['cabinet_id_dest'])){
    

    $token_id=$Gtable->clear($_POST['token_id']);
    $cabinet_id_source=$Gtable->clear($_POST['cabinet_id_source']);
    $cabinet_id_dest=$_POST['cabinet_id_dest'];
    $cabinet_id_rules=$_POST['cabinet_id_rules'];
    $tokenText=$Gtable->getBystr('Tokens','id='.$token_id);
    if (count($tokenText)==1){
        $autoRulesCop=new AutoRulesCopier($tokenText[0]['token'],$apiAddress,$tokenText[0]['proxy_url'],$tokenText[0]['proxy_auth'],$tokenText[0]['useragent']);
        $cabinet_val=$autoRulesCop->Download($cabinet_id_source);
        $cols = 'token_id, cabinet_id, cabinet_val, operation, cabinet_dest_id';
        $cabinet_id_deststr=implode(",",$cabinet_id_dest);
        $vals = "{$token_id},'{$cabinet_id_source}','{$cabinet_val}',0,'{$cabinet_id_deststr}'";
        $Gtable->Add('cabinets',$cols,$vals);
        $cabinet_val=json_decode(base64_decode($cabinet_val),true);
        $cabinet_val=$cabinet_val['data'];
        foreach ($cabinet_id_dest as $cabinet_id){
            foreach ($cabinet_val as $ck){
                $dataTemp=[];
                if (in_array($ck['id'],$cabinet_id_rules)) {
                    $dataTemp['name']=$ck['name'];
                    $dataTemp['schedule_spec']=$ck['schedule_spec'];
                    $dataTemp['evaluation_spec']=$ck['evaluation_spec'];
                    $dataTemp['execution_spec']=$ck['execution_spec'];
                    echo $autoRulesCop->Upload($cabinet_id,$dataTemp);
                }
            }
        }
        Alert('Правила скопированы');
    }else
        Alert('Не верный токен!');
}
loadV('copy');