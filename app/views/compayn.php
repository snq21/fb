<?php
global $Gtable;
global $val;
$tokens=$Gtable->getBystr('Tokens',"u_id={$_SESSION['uid']} and status=0");

if (count($tokens)<1){
    Alert('У вас необнаружен не один токен, добавьте токен!');
}
?>
<div class="container-fluid">
    <div class="row p-5">
        <div class="col-md-12">
            <div class="row justify-content-center">
                <div class="col-md-8 offset-1">
                    <form action="" method="post" class="form form-group  rounded badge-light p-5">
                        <h5 class="text-center">Копировать настройки кабинета</h5>
                        <div class="form-group">Выберите токен из списка
                            <select name="token_id" id="token_id" placeholder="Названия токена" class="form-control" required>
                                <?php
                                $tokens_option="<option value=''>Выбор</option>";
                                foreach ($tokens as $token){
                                    $tokens_option.="<option value='{$token['id']}'>{$token['title']}</option>";
                                }
                                ?>
                                <?=$tokens_option?>
                            </select>

                        </div>
                        <div class="form-group">ID Кабинета с которого копируем
                            <select class="form-control" type="text" placeholder="ID кабинета с которого копируем" id="cabinet_id_source" name="cabinet_id_source" required="">
                            </select>
                        </div>
                        <div class="form-group">Компании
                            <select class="form-control" type="text" multiple placeholder="Перечень правил" id="cabinet_id_companyes" name="cabinet_id_companyes[]" required="">
                            </select>
                        </div>
                        <input type="submit" value="Скачать" class="btn btn-primary btn-block">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    
    $("#cabinet_id_source" ).change(function() {
        $("#cabinet_id_companyes option[value='']").hide();    
        var options='<option value="">Выбор</option>';
        $.ajax({url: "/compayn?json=1&token_id="+$("#token_id").val()+'&account_id='+$(this).val(),    dataType : "json"}).done(function(row) {
            if(row['data']){
                for(var item in row['data']){
                    var t=row['data'];
                    options+='<option value="'+t[item]['id']+'">'+t[item]['name']+'</option>';
                }
                    $("#cabinet_id_companyes").html(options);
   
            }else{
                alert('Возможно ваш токен не действителен');
            }
            console.log(row['data']);
          });
          
    });

    $( "#token_id" ).change(function() {
        var options='<option value="">Выбор</option>';
        $.ajax({url: "/copy?json=1&get-token_id="+$(this).val(),    dataType : "json"}).done(function(row) {

            if(row['data']){
                for(var item in row['data']){
                    var t=row['data'];
                    options+='<option value="'+t[item]['account_id']+'">'+t[item]['name']+'('+t[item]['account_id']+')</option>';
                }
                    $("#cabinet_id_dest").html(options);
                    $("#cabinet_id_source").html(options);
            
   
            }else{
                alert('Возможно ваш токен не действителен');
            }
          });
    });    
});
$('body').css('background-color','#17a2b8');
</script>
