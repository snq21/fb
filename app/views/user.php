<?php
 global $Gtable;
 global $val;
 if (isset($_GET['uid'])) {
 	$id=(int)$_GET['uid'];
 	  $val=$Gtable->getBystr('Users','id='.$id);
	 if (count($val)==1) {
	 	 $val=$val[0];
	 }else{
	 	$val=['fio'=>null,'login'=>null,'status'=>null];
	 	Alert('Не верный ID');
	 	unset($_GET['uid']);
	}
 }else{
 		$val=['fio'=>null,'login'=>null,'status'=>null];
 }
 
?>

<div class="container-fluid">
	<div class="row p-5">
		<div class="col-md-12">
			<div class="row justify-content-center">
				<div class="col-md-6 offset-1">
					<form action="" method="post" class="form form-group  rounded badge-light p-5">
					<h5 class="text-center"><?=isset($_GET['uid'])? 'Обновить данные': 'Добавить'?> сотрудника</h5>
					<div class="form-group">
						<input  class="form-control" type="text" placeholder="ФИО" value="<?=$val['fio']?>" name="fio" required="">
					</div>
					<div class="form-group">
						<input  class="form-control" type="text" placeholder="Логин" value="<?=$val['login']?>" name="login" required="">
					</div>
					<div class="form-group">
						 <input  class="form-control" type="password" placeholder="Пароль"  name="password" required="">	
					</div>
					<div class="form-group">
						 <input  class="form-control" type="password" placeholder="Повтор паролья" name="password2" required="">	
					</div>
						<?=isset($_GET['uid'])? '<input type="hidden" name="uid" value='.(int)$_GET['uid'].'>': ''?>
					<input type="submit" value="<?=isset($_GET['uid'])? 'Обновить': 'Добавить'?>" class="btn btn-primary btn-block">
				</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('body').css('background-color','#17a2b8');
</script>