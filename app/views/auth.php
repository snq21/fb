<div class="container-fluid">
	<div class="row p-5">
		<div class="col-md-12">
			<div class="row justify-content-center">
				<form action="" method="post" class="form form-group  rounded badge-light p-5">
					<h5 class="text-center">Вход</h5>
					<div class="form-group">
						<input  class="form-control" type="text" placeholder="Логин" name="login" required="">
					</div>
					<div class="form-group">
						 <input  class="form-control" type="password" placeholder="Пароль" name="pass" required="">	
					</div>	
					<input type="submit" value="Вход" class="btn btn-primary btn-block">
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	$('body').css('background-color','#17a2b8');
</script>