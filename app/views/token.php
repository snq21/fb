<?php
global $Gtable;
global $val;
$val=[ 'useragent'=>$_SERVER['HTTP_USER_AGENT'],'token'=>null,'proxy_url'=>null,'proxy_auth'=>null, 'title'=>'Токен 1', 'status'=>null, 'created_at'=>null, 'updated_at'=>null ];
if (isset($_GET['tid'])) {
    $id=(int)$_GET['tid'];
    $val=$Gtable->getBystr('Tokens','id='.$id);
    if (count($val)==1) {
        $val=$val[0];
    }else{
        Alert('Не верный ID');
        unset($_GET['tid']);

    }
}
?>

<div class="container-fluid">
    <div class="row p-5">
        <div class="col-md-12">
            <div class="row justify-content-center">
                <div class="col-md-6 offset-1">
                    <form action="" method="post" class="form form-group  rounded badge-light p-5">
                        <h5 class="text-center"><?=isset($_GET['tid'])? 'Обновить данные': 'Добавить'?> токена</h5>
                        <div class="form-group">Названия токена
                            <input type="text" name="title" placeholder="Названия токена" class="form-control" required value="<?=$val['title']?>">
                        </div>
                        <div class="form-group">Токен
                            <textarea name="token" class="form-control" placeholder="Токен" required><?=$val['token']?></textarea>
                        </div>
                        <div class="form-group">Адрес прокси сервера
                            <input type="text" id='proxy_url' name="proxy_url" placeholder="Адрес прокси сервера" class="form-control" required value="<?=$val['proxy_url']?>">
                        </div>
                        <div class="form-group">Данные авторизации прокси
                            <textarea name="proxy_auth" id='proxy_auth' class="form-control" placeholder="Данные авторизации прокси" ><?=$val['proxy_auth']?></textarea>
                        </div>
                        <div class="form-group">User agent
                            <textarea name="useragent" id='useragent' class="form-control" placeholder="User agent" ><?=$val['useragent']?></textarea>
                        </div>
                        <div class="form-group">Проверить прокси сервер
                            <button class="btn btn-primary" onclick='check();return false;'>Проверить</button>
                        </div>
                        <?=isset($_GET['tid'])? '<input type="hidden" name="tid" value='.(int)$_GET['tid'].'>': ''?>
                        <input type="submit" value="<?=isset($_GET['tid'])? 'Обновить': 'Добавить'?>" class="btn btn-primary btn-block">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function check() {
        var purl=$('#proxy_url').val();
        var pauth=$('#proxy_auth').val();
        var url='/proxy?proxy-url='+purl+'&json=true';
        if (pauth) {
            var url='/proxy?proxy-url='+purl+'&proxy-auth='+pauth+'&json=true';
            
        }
        $.ajax({url: url,    dataType : "text"}).done(function(row) {
            alert(row);
          });
    

    }
    $('body').css('background-color','#17a2b8');
</script>
