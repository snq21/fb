<style>
    .hide{
        display: none !important;
    }
</style>
<?php
global $table;
global $no_of_records_per_page;
global $offset;
global $counter;
global $page;
global $total_pages;

?>

<div class="container-fluid p-0">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row justify-content-center">
                <div class="col-md-3 align-items-center p-1">
                    <form action="" class=" form-inline p-1" method="post">
                        <div class="form-group pr-1">
                            <input class="form-control" type="text" name="search" placeholder="Текс">
                        </div>
                        <div class="form-group">
                            <input class="btn btn-outline-primary" type="submit" value="Найти">
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <table class="table table-dark table-bordered">
                        <?=$table;?>
                    </table>
                </div>
            </div>
            <div class="row <?=$counter<=$no_of_records_per_page ? "hide": "";?>">
                <div class="col-md-12">
                    <div class="row justify-content-center">
                        <nav  aria-label="Навигация">
                            <ul class="pagination text-center">
                                <?php
                                for ($i=0; $i <$counter ; $i++) {
                                    if ($page==($i+1)) {
                                        echo '<li class="page-item active"><a class="page-link" href="?page='.($i+1).'">'.($i+1).'</a></li>';
                                    }else{
                                        echo '<li class="page-item"><a class="page-link" href="?page='.($i+1).'">'.($i+1).'</a></li>';

                                    }
                                }
                                ?>

                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
