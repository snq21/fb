<?php
class  AutoRulesCopier{
    private $accessToken;
    private $apiAddress;
    private $query;
    private $fbFeilds;
    private $proxy;
    private $proxyauth;
    private $useragent;

    public function __construct($accessToken,$apiAddress,$proxyServer="",$proxyauth="",$useragent=null )
    {
        $this->accessToken=$accessToken;
        $this->apiAddress=$apiAddress;
        $this->query=$this->apiAddress;
        $this->fbFeilds="fields=entity_type,evaluation_spec,execution_spec,name,schedule_spec";
        $this->proxy=$proxyServer;
        $this->proxyauth=$proxyauth;
        $this->useragent=$useragent;
    }
    public function queryHttp($url,$data=[],$isDelete=false)
    {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            if ($this->useragent!=null) {
                curl_setopt($ch, CURLOPT_USERAGENT, $this->useragent);
            }

            curl_setopt($ch, CURLOPT_PROXY, $this->proxyauth.'@'.$this->proxy);
            if (count($data)>0&$isDelete==false) {
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            }
            if ($isDelete!=false) {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE"); 
            }
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            
            $curl_scraped_page = curl_exec($ch);

                if($curl_scraped_page === false)
                {
                    $curl_scraped_page=curl_error($ch);
                }
            curl_close($ch);

            return $curl_scraped_page;
        
    }
    public function Download($id){
        return base64_encode($this->queryHttp($this->query."act_{$id}/adrules_library?access_token={$this->accessToken}&{$this->fbFeilds}"));
    }
    
    public  function GetAccounts(){
        
        return $this->queryHttp($this->query."me/adaccounts?fields=name,account_id&access_token={$this->accessToken}");
    }
    public  function GetCompaigns($id){
        
        return $this->queryHttp($this->query."act_{$id}/campaigns?fields=adlabels,bid_strategy,budget_rebalance_flag,buying_type,daily_budget,execution_options,iterative_split_test_configs,lifetime_budget,name,objective,promoted_object,source_campaign_id,spend_cap,status,topline_id,upstream_events&access_token={$this->accessToken}");
    }

    
    
    public function Upload($id,$data){
        return    $this->queryHttp($this->query."act_{$id}/adrules_library?access_token={$this->accessToken}",$data);
    }
    public function Delete($id,$data){
        return    $this->queryHttp($this->query."{$id}?access_token={$this->accessToken}",$data,true);
    }
//ННастраиваем интерфейс AdsManager раз и навсегда!

    public function DownloadInterface($id,$fields=""){
        if (strlen($fields)>0) {
            $this->fbFeilds="fields={$fields}";
        }else{
            $this->fbFeilds="fields=user_settings{column_presets{columns,id,name}}";
        }
        
        return ($this->queryHttp($this->query."act_{$id}?access_token={$this->accessToken}&{$this->fbFeilds}"));
    }

    public function UploadColumn($id,$data){
        return    $this->queryHttp($this->query."{$id}/column_presets/?access_token={$this->accessToken}",$data);
    }
    public function DeleteColumn($id){
        return    $this->queryHttp($this->query."{$id}/column_presets?access_token={$this->accessToken}",[],true);
    }


    public function UploadInterface($id,$data){
        return    $this->queryHttp($this->query."act_{$id}/user_settings/?access_token={$this->accessToken}",$data);
    }
}
