<?php
require_once 'root.php';

if(isset($_GET['msg'])){
    Alert(htmlspecialchars($_GET['msg']));
}

session_start();
error_reporting(E_ALL);
    ini_set('display_errors', 1);



if (!isset($_GET['json'])) {
    

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Копирование автоправил FB</title>
    <link rel="stylesheet" href="/public/css/bootstrap.min.css">
    <script src="/public/js/jquery-3.4.1.min.js"></script>
    <script src="/public/js/bootstrap.min.js"></script>
</head>
<body class="badge-primary">
<?php
if (isset($_SESSION['role'],$_SESSION['uid'])) {
    ?>
    <div class="container-fluid p-0">
        <div class="row p-0">
            <div class="col-md-12 p-0">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="navbar-toggler-icon"></span>
                    </button> <a class="navbar-brand" href="/profile">FB</a>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link" href="/tokens">Токены<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/token">Новый токен</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/history">история</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/proxy">Прокси сервера</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown">Шаблоны и автоправила</a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            
                                <a class="dropdown-item" href="/copy">Копировать автоправила</a>
                            
                            
                                <a class="dropdown-item" href="/tcolumn">Копировать шаблоны</a>
                            
                            
                                <a class="dropdown-item" href="/delete">Удалить автоправила</a>
                            
                            
                                <a class="dropdown-item" href="/tcerace">Удалить шаблоны</a>

                                </div>
						    </li>


                            <?php
                            if ($_SESSION['role']==1) {
                                ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="/user">Новый сотрудник</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/users">Сотрудники</a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                        <ul class="navbar-nav ml-md-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="/logaut">Выход <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <?php
}
}
if (isset($_SESSION['uid'],$_GET['proxy'])) {
        $file=file_get_contents($_GET['proxy']);
        if (strlen($file)>0) {
            file_put_contents('proxy.txt',$file);
        }
       redirect('/proxy?msg=Список прокси серверов обновлены!');
}
if (isset($_SERVER['PATH_INFO'])) {
    $url=$_SERVER['PATH_INFO'];
    if (!isset($_SESSION['uid'])) {
        loadM('auth');
    }else{
        loadM(basename($url));
    }
}else{
    if (isset($_SESSION['uid'])) {
        redirect('/profile');
    }else
        redirect('/auth');
}
if (!isset($_GET['json'])) {
 
?>
</body>
</html>
<?php
}
?>